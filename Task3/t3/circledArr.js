function circleArray() {
    const items = [];
  
    return {
        add,
        get
    };
  
    function add(element) {
        items.push(element);
    }
  
    function get(i) {
        let max = items.length;
        return items[(i % max + max) % max];
    }
}

// let circledArray = new circleArray();

// circledArray.add("Київ");
// circledArray.add("Харків");
// circledArray.add("Херсон");

// console.log(circledArray.get(0)); // Київ
// console.log(circledArray.get(4)); // Харків
// console.log(circledArray.get(-1)); // Херсон
