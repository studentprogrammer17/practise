function Rectangle() {
    let rect = document.getElementById("rect");
    let context = rect.getContext('2d');
    context.fillStyle='#fa4b2a';   
    context.fillRect(20, -20, 30, 70); 
    
}

function Triangle() {
    let trian = document.getElementById("triangle");
    let context = trian.getContext("2d");
  
    context.beginPath();
    context.moveTo(30, 90); 
    context.lineTo(90, 90); 
    context.lineTo(60, 40);
    context.closePath();

    context.fillStyle = "#FFCC00";
    context.fill();
  }