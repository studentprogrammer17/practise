function testString(string)
{
    let parenth = [];
    let i = 0;
    for(let el of string) {
        if (el === '(' || el === '[' || el === '{') {
            parenth[i] = el;
            i++;
            continue;
        }
        let check;
        if(el === ')') {
            check = parenth.pop();
            if (check === '[') {
                console.log('false');
                return false;
            }
        }
 
        if(el === ']') {
            check = parenth.pop();
            if (check === '(') {
                console.log('false');
                return false;
            }
        }
        i = 0;
    }
    console.log('true');
    return true;
}

// testString("isu([syvstc]ts(crs))cs"); // must be true
// testString("isu[syv(stc]ts(crs))cs"); // must be false
