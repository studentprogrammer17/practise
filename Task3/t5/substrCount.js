function substrCount(input,needle,offset,length) {
    let count = 0;
    for(let i = offset; i < length + offset; i++) {
        if(input[i] + input[i+1] === needle && i < length + offset - 1) count++;
    }
    return count;
}

// let a = substrCount('Good Golly Miss Molly', 'll', 7,10);
// console.log(a);