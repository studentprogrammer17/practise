// this is for menu when make display less

let menuItems = document.getElementById("menuItems");
menuItems.style.maxHeight = "0px";

function menuTo() {
    if(menuItems.style.maxHeight == "0px") {
        menuItems.style.maxHeight = "200px";
    }
    else {
        menuItems.style.maxHeight = "0px";
    }
}


// register form check

function checkReg() {
    let username = document.getElementById("usernameReg");
    let email = document.getElementById("emailReg");
    let password = document.getElementById("passwordReg");
    let letters = /^[A-Za-z]+$/;
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
    if(!username.value.match(letters) || username.value.length >= 20 || username.value.length < 4) {
        alert('Логін має бути на латиниці та не менш ніж 4 символа');
        username.focus();
        return false;
    }

    else if (password.value.length == 0 || password.value.length >= 20 || password.value.length < 6) {
        alert("Треба заповнити поле пароля ( не менш 6 символів)");
        password.focus();
        return false;
    }
    
    else if(!email.value.match(mailformat)) {
        alert("Невірно  введений email");
        email.focus();
        return false;
    }

    else {
        alert("Ви були зареєстровані");
        return true;
    }
}

// login form check

function checkLogin() {
    let username = document.getElementById("usernameLog");
    let password = document.getElementById("passwordLog");
    let letters = /^[A-Za-z]+$/;
    
    if(!username.value.match(letters) || username.value.length >= 20 || username.value.length < 4) {
        alert('Логін має бути на латиниці та не менш ніж 4 символа');
        return false;
    }

    else if (password.value.length == 0 || password.value.length >= 20 || password.value.length < 6) {
        alert("Треба заповнити поле пароля ( не менш 6 символів)");
        return false;
    }

    else {
        alert("Ви війшли!");
        return true;
    }
}


// sorting goods

(function() {	
    let field = document.querySelector('.items');
    let li = Array.from(field.children);

    function FilterProduct() {
        for(let i of li){
            const name = i.querySelector('strong');
            const x = name.textContent;
            i.setAttribute("data-category", x);
        }

        let indicator = document.querySelector('.logo').children;

        this.run = function() {
            for(let i=0; i<indicator.length; i++)
            {
                indicator[i].onclick = function () {
                    for(let x=0; x<indicator.length; x++)
                    {
                        indicator[x].classList.remove('active');
                    }
                    this.classList.add('active');
                    const displayItems = this.getAttribute('data-filter');

                    for(let z=0; z<li.length; z++)
                    {
                        li[z].style.transform = "scale(0)";
                        setTimeout(()=>{
                            li[z].style.display = "none";
                        }, 500);

                        if ((li[z].getAttribute('data-category') == displayItems) || displayItems == "all")
                         {
                             li[z].style.transform = "scale(1)";
                             setTimeout(()=>{
                                li[z].style.display = "block";
                            }, 500);
                         }
                    }
                };
            }
        }
    }

    function SortProduct() {
        let select = document.getElementById('select');
        let ar = [];
        for(let i of li){
            const last = i.lastElementChild;
            const x = last.textContent.trim();
            const y = Number(x.substring(1));
            i.setAttribute("data-price", y);
            ar.push(i);
        }
        this.run = ()=>{
            addevent();
        }
        function addevent(){
            select.onchange = sortingValue;
        }
        function sortingValue(){
        
            if (this.value === 'Default') {
                while (field.firstChild) {field.removeChild(field.firstChild);}
                field.append(...ar);	
            }
            if (this.value === 'LowToHigh') {
                SortElem(field, li, true)
            }
            if (this.value === 'HighToLow') {
                SortElem(field, li, false)
            }
        }
        function SortElem(field,li, asc){
            let  dm, sortli;
            dm = asc ? 1 : -1;
            sortli = li.sort((a, b)=>{
                const ax = a.getAttribute('data-price');
                const bx = b.getAttribute('data-price');
                return ax > bx ? (1*dm) : (-1*dm);
            });
             while (field.firstChild) {field.removeChild(field.firstChild);}
             field.append(...sortli);	
        }
    }

    new FilterProduct().run();
    new SortProduct().run();
})();
		
   
