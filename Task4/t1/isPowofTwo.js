function isPowOfTwo(n) {
    let x = n;

    if(x === 0) return false;

    while(x % 2 == 0){
        x = x / 2;
    }

    if(x === 1) {
        console.log(`${n} is power of 2`);
        return true;
    }

    else {
        console.log(`${n} is NOT power of 2`);
        return false;
    }
}

// isPowOfTwo(4); // true
// isPowOfTwo(16); // true
// isPowOfTwo(15); // false
// isPowOfTwo(10); // false