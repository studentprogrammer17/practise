function testDate(date){
    let regex = /^\d|\d\d[-]\d|dd[-]\d\d\d\d$/;
    let isDate = regex.test(date);
    if(isDate) {
        return true;
    }
    else {
        return false;
    }
}

console.log(testDate("25-07-2021"));
console.log(testDate("25-7-2021"));
console.log(testDate("1-1-2021"));
console.log(testDate("2-12-1979"));

