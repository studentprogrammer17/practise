function editStr(str) {
    str = str.replaceAll(',', '')
    let mapObj = {Lennon:"John",John:"Lennon",
            McCartney:"Paul",Paul:"McCartney",
            Harrison:"George",George:"Harrison",
            Star:"Ringo ",Ringo:"Star"};

    let re = new RegExp(Object.keys(mapObj).join("|"),"gi");
    str = str.replace(re, function(matched){  
        return mapObj[matched];
    });
    return str;
}

let names = "Lennon, John\nMcCartney, Paul\nHarrison, George\nStar,Ringo";
console.log(editStr(names));

