function findWord(string,sub) {
    let answer = '';
    let check = false;
    let check_start = true;
    let check_end = true;
    let start_i = 0;
    let end_i = 0;

    for(let i = 0, k = 0; i < string.length; i++) {
        if(sub[k] == string[i]) {
            for(let j = 0; j < sub.length; j++, i++) {
                if(string[i] === sub[j]) {
                    if(check_start == true) {
                        check_start = false;
                        start_i = i;
                    }
                    check = true;
                }
                else {
                    check = false;
                    continue;
                }
                if(check_end == true) {
                    check_end = false;
                    end_i = i;
                }
            }
        }
        check_end = true;
        check_start = true;
        if(check == true) {
            for(let m = start_i; m < string.length; m++) {
                if(string[m] === ' ') {
                    end_i = m;
                    break;
                }
            }
            for(let p = start_i; p > 0; p--) {
                if(string[p] === ' ') {
                    start_i = p;
                    break;
                }
            }
            for(let l = start_i + 1; l < end_i; l++) {
                answer += string[l];
            }
            return answer;
        }
        check = false;
    }
}

// console.log(findWord("Ми знаємо, що монохромний колір – це градації сірого", "хром")); // return монохромний
// console.log(findWord("I live tr ry in a beuatiful country you known, that is - Ukraine!", "try")); // return country