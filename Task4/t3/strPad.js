function strPad(input, fullLen, fillStr, fillType) {
    let answer = '';
    let sizeFillStr = fillStr.length;
    switch(fillType) {
        case undefined || 'FILL_RIGHT':
            answer += input;
            for(let i = 0, j = 0; i < fullLen - input.length; i++,j++) {
                if(j === sizeFillStr) j = 0;
                answer += fillStr[j];
            }
            return answer;
            break;
        case 'FILL_LEFT':
            for(let i = 0, j = 0; i < fullLen - input.length; i++,j++) {
                if(j === sizeFillStr) j = 0;
                answer += fillStr[j];
            }
            answer += input;
            return answer;
            break;
        case 'FILL_BOTH':
            for(let i = 0, j = 0; i < (fullLen - input.length) / 2; i++,j++) {
                if(j === sizeFillStr) j = 0;
                answer += fillStr[j];
            }
            answer += input;
            for(let i = 0, j = 0; i < (fullLen - input.length) / 2; i++,j++) {
                if(j === sizeFillStr) j = 0;
                answer += fillStr[j];
            }
            return answer;
            break;
        default:
            console.log("Input in fillType: nothing or - FILL_RIGHT, FILL_LEFT, FILL_BOTH");
            return false;
    }
}

// console.log(strPad("star", 10, "_*_", "FILL_RIGHT")); // return star_*__*_;
// console.log(strPad("star", 8, "_*_", "FILL_LEFT")); // return _*__star;
// console.log(strPad("star", 8, "*", "FILL_BOTH")); // return **star**
// console.log(strPad("star", 8, "*", "zxkdaskd")); // return false
